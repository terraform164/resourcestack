terraform {
  backend "http" {} 
}

module "aws_infra_stack" {
  source = "../modules-aws/"
  tags = var.tags
  env = var.env
  ami = var.ami
  instance_type = var.instance_type
  
}
